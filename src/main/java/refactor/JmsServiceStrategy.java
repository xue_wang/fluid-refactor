package refactor;

/**
 * Created by 24652 on 2018/3/8.
 *
 * 接口jms策略，针对不同的mq,如activeMq
 */
public interface JmsServiceStrategy {

  JmsServiceStrategy bindToBrokerAtUrl(String url)throws Exception;

  JmsServiceStrategy andThen();

  JmsServiceStrategy sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);

  long getEnqueuedMessageCountAt(String aDestinationName) throws Exception;

  String retrieveASingleMessageFromTheDestination(String aDestinationName);

  String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);

}
