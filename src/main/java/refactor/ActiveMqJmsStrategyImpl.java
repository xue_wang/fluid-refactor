package refactor;


import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Created by 24652 on 2018/3/8.
 *
 * 实现 activeMq
 */
public class ActiveMqJmsStrategyImpl implements  JmsServiceStrategy{

    private static final Logger LOG = getLogger(ActiveMqJmsStrategyImpl.class);

    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private String brokerUrl;

    @Override
    public JmsServiceStrategy bindToBrokerAtUrl(String brokerUrl)throws Exception {
        return this;

    }

    @Override
    public JmsServiceStrategy andThen() {
        return this;
    }

    @Override
    public JmsServiceStrategy sendATextMessageToDestinationAt(String aDestinationName, String aMessageToSend) {

        return this;
    }

    @Override
    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return 0;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return null;
    }

    @Override
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, int aTimeout) {
        return null;
    }
}
