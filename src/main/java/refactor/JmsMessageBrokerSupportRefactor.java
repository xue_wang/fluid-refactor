package refactor;

/**
 * Created by 24652 on 2018/3/8.
 *
 */
public class JmsMessageBrokerSupportRefactor {

    private JmsServiceStrategy jmsServiceStrategy;

    public JmsServiceStrategy getJmsServiceStrategy() {
        return jmsServiceStrategy;
    }

    public void setJmsServiceStrategy(JmsServiceStrategy jmsServiceStrategy) {
        this.jmsServiceStrategy = jmsServiceStrategy;
    }

    JmsServiceStrategy bindToBrokerAtUrl(String url) throws Exception{
        return jmsServiceStrategy.bindToBrokerAtUrl(url);
    }

    JmsServiceStrategy andThen(){
        return jmsServiceStrategy;
    }

    JmsServiceStrategy sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend){
        return jmsServiceStrategy.sendATextMessageToDestinationAt(aDestinationName,aMessageToSend);
    }

    long getEnqueuedMessageCountAt(String aDestinationName) throws Exception{
        return jmsServiceStrategy.getEnqueuedMessageCountAt(aDestinationName);
    }

    String retrieveASingleMessageFromTheDestination(String aDestinationName){
        return jmsServiceStrategy.retrieveASingleMessageFromTheDestination(aDestinationName);
    }

    String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout){
        return jmsServiceStrategy.retrieveASingleMessageFromTheDestination(aDestinationName,aTimeout);
    }
}
